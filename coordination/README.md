# Dashboard for Chapter Coordination

This is a jupyter notebook that shows some graphs related to the CCC member
pipeline, as well as some stats on the mailchimp.

It is currently deployed at https://cccdash-internal.herokuapp.com/

## Env variables
Set `AIRTABLE_API_KEY` and `MAILCHIMP_API_KEY` in the environment or a .env
file.

## Run in container
From `deployment/` create a .env file and:
```
docker build -t $USER/jupyter-dashboard .
docker run --rm -it -p 8888:8888 -p 8866:8866 -v (pwd)":/home/jovyan" --env-file .env $USER/jupyter-dashboard
```

## Deployment
You can deploy the dashboard as a standalone server using voila. It is currently
configured to be deployed to heroku. Note that the default heroku install is
world-visible, though arbitary code execution is disabled. To deploy a secure
dashboard, you would need to deploy voila into a secure subnet and use something
like nginx and JWT to secure access (maybe https://github.com/auth0/nginx-jwt)

To deploy on heroku, use `heroku config:set` to set the needed envs.
